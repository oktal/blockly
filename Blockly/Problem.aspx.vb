﻿Public Class Problem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Protected Sub Blockly_Ready() Handles BlocklyControl.Ready
        SqlDataSourceSolution.ConnectionString = "data source=" + Server.MapPath(BlocklyControl.Database)
        Dim solutionTable = CType(SqlDataSourceSolution.Select(New DataSourceSelectArguments), DataView).Table
        Dim answerTable = BlocklyControl.ResultContainer.Data
        If solutionTable.Rows.Count = answerTable.Rows.Count And solutionTable.Columns.Count = answerTable.Columns.Count Then
            Success = "true"
            For i = 0 To solutionTable.Rows.Count - 1
                For j = 0 To solutionTable.Columns.Count - 1
                    If Not solutionTable.Rows(i)(j).Equals(answerTable.Rows(i)(j)) Then Success = "false"
                Next
            Next
        Else
            Success = "false"
        End If

        Try
            DataBind()
        Catch ex As SQLite.SQLiteException
        End Try
    End Sub

    Protected Property Success As String

End Class