﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Blockly.ascx.vb" Inherits="Blockly.Blockly" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxTk" %>

<script src="google-blockly/blockly_compressed.js"></script>
<script src="google-blockly/blocks_compressed.js"></script>
<script src="google-blockly/msg/js/en.js"></script>

<form id="form1" runat="server">
    <table><tr>
        <td>
            <div id="blocklyDiv" style="height:500px;width:1000px"></div>
        </td>
        <td style="vertical-align:top">
            <ajaxTk:TabContainer ID="TabContainer1" runat="server">
                <ajaxTk:TabPanel ID="TabPanel1" runat="server" HeaderText="<%# Tables.ElementAtOrDefault(0) %>" Visible="<%# Tables.Count > 0 %>">
                    <ContentTemplate>
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSourceTable1" Font-Size="Smaller"></asp:GridView>
                    </ContentTemplate>
                </ajaxTk:TabPanel>
                <ajaxTk:TabPanel ID="TabPanel2" runat="server" HeaderText="<%# Tables.ElementAtOrDefault(1) %>" Visible="<%# Tables.Count > 1 %>">
                    <ContentTemplate>
                        <asp:GridView ID="GridView2" runat="server" DataSourceID="SqlDataSourceTable2" Font-Size="Smaller"></asp:GridView>
                    </ContentTemplate>
                </ajaxTk:TabPanel>
                <ajaxTk:TabPanel ID="TabPanel3" runat="server" HeaderText="<%# Tables.ElementAtOrDefault(2) %>" Visible="<%# Tables.Count > 2 %>">
                    <ContentTemplate>
                        <asp:GridView ID="GridView3" runat="server" DataSourceID="SqlDataSourceTable3" Font-Size="Smaller"></asp:GridView>
                    </ContentTemplate>
                </ajaxTk:TabPanel>
            </ajaxTk:TabContainer>
        </td>
    </tr></table>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table><tr>
                <td style="width:1000px;vertical-align:top">
                    <asp:PlaceHolder ID="TutoriaTemplatePlaceHolder" runat="server"></asp:PlaceHolder>
                </td>
                <td style="vertical-align:top;width:calc(100vw - 1025px);height:calc(100% - 525px);position:absolute;overflow:auto">
                    <asp:GridView ID="GridView" runat="server" DataSourceID="SqlDataSourceOutput"></asp:GridView>
                </td>
            </tr></table>
            <asp:SqlDataSource ID="SqlDataSourceOutput" runat="server" ProviderName="System.Data.SQLite"></asp:SqlDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
</form>

<asp:SqlDataSource ID="SqlDataSourceTables" runat="server" ProviderName="System.Data.SQLite"
    SelectCommand="SELECT name FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%'"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceColumns" runat="server" ProviderName="System.Data.SQLite"
    SelectCommand="SELECT sql FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%'"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceTable1" runat="server" ProviderName="System.Data.SQLite"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceTable2" runat="server" ProviderName="System.Data.SQLite"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceTable3" runat="server" ProviderName="System.Data.SQLite"></asp:SqlDataSource>

<xml id="toolbox" style="display:none"><%# ToolboxXml.Item("toolbox").InnerXml %></xml>
<xml id="tableTools" style="display:none"><%# ToolboxXml.SelectSingleNode("//category[@name = ""Tables""]").InnerXml %></xml>
<xml id="columnTools" style="display:none"><%# ToolboxXml.SelectSingleNode("//category[@name = ""Columns""]").InnerXml %></xml>

<div id="blocks" style="display:none"><%# BlocksJson %></div>

<script>
    // Initialize the Blockly component
    var json = document.getElementById('blocks').innerText;
    var blocks = JSON.parse(json);
    var tableNodes = document.getElementById('tableTools').children;
    var columnNodes = document.getElementById('columnTools').children;
    for (var i = 0; i < tableNodes.length; i++) {
        var type = tableNodes[i].getAttribute("type");
        if (/^_tbl_/.test(type)) {
            blocks[type] = {
                "message0": type.replace(/^_tbl_/, "") + " %1",
                "args0": [{ "type": "input_value", "name": "next", "check": "Table" }],
                "output": "Table",
                "colour": 180
            };
        }
    }
    for (var i = 0; i < columnNodes.length; i++) {
        var type = columnNodes[i].getAttribute("type");
        if (/^_col_/.test(type)) {
            blocks[type] = {
                "message0": type.replace(/^_col_/, "") + " %1",
                "args0": [{ "type": "input_value", "name": "next", "check": "Expression" }],
                "output": "Expression",
                "colour": 60
            };
        }
    }
    for (var blockName in blocks) {
        (function () {
            var block = blocks[blockName];
            Blockly.Blocks[blockName] = { init: function () { this.jsonInit(block); } };
        })();
    }
    var workspace = Blockly.inject('blocklyDiv', { toolbox: document.getElementById('toolbox') })
    workspace.addChangeListener(onWorkspaceChange);

    function onWorkspaceChange(event)
    {
        // Extract the XML representing the user's SQL query, generated by the Blockly component.
        var xml = Blockly.Xml.workspaceToDom(workspace);
        var xmlText = Blockly.Xml.domToText(xml);

        // Fire a postback to the server with the XML data, to refresh the output view.
        Sys.WebForms.PageRequestManager.getInstance().beginAsyncPostBack([ '<%# UpdatePanel1.UniqueID %>' ], '<%# UpdatePanel1.UniqueID %>', '_Blockly ' + xmlText);
    };
</script>
