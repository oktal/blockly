﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Fading.aspx.vb" Inherits="Blockly.Fading" %>
<%@ Register Src="~/Blockly.ascx" TagName="Blockly" TagPrefix="uh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="max-height:100vh;overflow:hidden">
    <uh:Blockly id="BlocklyControl" runat="server" Toolbox="_all_clauses,_all_tables,_all_columns" Database="Test.db">
        <TutorialTemplate>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View0" runat="server">
                    <p>In this example problem we will be creating a SQL query that will retrieve the telephone numbers from
                    all the records in the People table, and list them in the order of their Birthdate.</p>

                    <p>Begin by locating the SELECT FROM block in the Clauses section of the toolbox. Drag it into the workspace.</p>
                </asp:View>
                <asp:View ID="View1" runat="server">
                    <p>Next locate the block representing the PhoneNumber column, which can be found under Expressions, Columns,
                    and drag this into the workspace and connect its left edge to the right edge of the SELECT FROM block.</p>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <p>Now locate the block representing the People table, which can be found under Tables,
                    and drag this into the workspace, connecting its left edge to the right edge of the SELECT FROM block.</p>
                </asp:View>
                <asp:View ID="View3" runat="server">
                    <p>Now find the ORDER BY block in the Clauses section, and drag this into the workspace,
                    connecting its top edge to the bottom edge of the SELECT FROM block.</p>
                </asp:View>
                <asp:View ID="View4" runat="server">
                    <p>Now find the block representing the Birthdate column, which can be found under Expressions, Columns,
                    and drag this into the workspace and connect its left edge to the right edge of the block where you think it should go.</p>
                </asp:View>
                <asp:View ID="View5" runat="server">
                    <asp:Label ID="SuccessLabel" runat="server" Text="Complete" Font-Size="XX-Large" ForeColor="#009900"></asp:Label>
                </asp:View>
            </asp:MultiView>
            <asp:Label ID="SqlQueryLabel" runat="server" Text="<%#: Container.SqlQuery %>" Font-Size="Large"></asp:Label>
        </TutorialTemplate>
    </uh:Blockly>
    <asp:Xml ID="Xml1" runat="server" Visible="False">
        <transitions>
            <condition destination="0">//b:xml[not(block)]</condition>
            <condition source="0" destination="1">//b:xml/b:block[@type="SELECT_FROM"]</condition>
            <condition source="1" destination="2">//b:xml/b:block[@type="SELECT_FROM"]/b:value[@name="expr_list"]/b:block[@type="_col_PhoneNumber"]</condition>
            <condition source="2" destination="3">//b:xml/b:block[@type="SELECT_FROM"]/b:value[@name="data_source"]/b:block[@type="_tbl_People"]</condition>
            <condition source="3" destination="4">//b:xml/b:block[@type="SELECT_FROM"]/b:next/b:block[@type="ORDER_BY"]</condition>
            <condition source="4" destination="5">//b:xml/b:block[@type="SELECT_FROM"]/b:next/b:block[@type="ORDER_BY"]/b:value[@name="expr_list"]/b:block[@type="_col_Birthdate"]</condition>
        </transitions>
    </asp:Xml>
</body>
</html>
