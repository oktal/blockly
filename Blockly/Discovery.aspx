﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Discovery.aspx.vb" Inherits="Blockly.Discovery" %>
<%@ Register Src="~/Blockly.ascx" TagName="Blockly" TagPrefix="uh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="max-height:100vh;overflow:hidden">
    <uh:Blockly id="BlocklyControl" runat="server" Toolbox="_all" Database="Test.db">
        <TutorialTemplate>
            <asp:Label ID="SqlQueryLabel" runat="server" Text="<%#: Container.SqlQuery %>" Font-Size="Large"></asp:Label>
            <br /><br />
            <asp:Label ID="SqlErrorLabel" runat="server" Text="<%#: Container.SqlError %>" Font-Size="Small" ForeColor="#FF6600"></asp:Label>
            <br />
            <asp:Label ID="XmlDebugLabel" runat="server" Text="<%#: Container.XmlOutput %>" Font-Size="XX-Small" ForeColor="#999999"></asp:Label>
        </TutorialTemplate>
    </uh:Blockly>
</body>
</html>
