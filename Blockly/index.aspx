﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="index.aspx.vb" Inherits="Blockly.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Example.aspx">Worked example</asp:LinkButton><br />
        <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/Fading.aspx">Faded example</asp:LinkButton><br />
        <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/Problem.aspx">Problem solving</asp:LinkButton><br />
        <asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="~/Discovery.aspx">Discovery learning</asp:LinkButton><br />
    </div>
    </form>
</body>
</html>
