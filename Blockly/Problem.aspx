﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Problem.aspx.vb" Inherits="Blockly.Problem" %>
<%@ Register Src="~/Blockly.ascx" TagName="Blockly" TagPrefix="uh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="max-height:100vh;overflow:hidden">
    <uh:Blockly id="BlocklyControl" runat="server" Toolbox="_all" Database="Test.db">
        <TutorialTemplate>
            See if you can create a SQL query that will retrieve the telephone numbers from all the records in the People table
            for which the length of the LastName field is 5 characters or less, and list them in the order of their Birthdate.
            <br /><br />
            <asp:Label ID="SqlQueryLabel" runat="server" Text="<%#: Container.SqlQuery %>" Font-Size="Large"></asp:Label>
            <br /><br />
            <asp:Label ID="SqlErrorLabel" runat="server" Text="<%#: Container.SqlError %>" Font-Size="Small" ForeColor="#FF6600"></asp:Label>
            <br />
            <asp:Label ID="SuccessLabel" runat="server" Text="Success" Font-Size="XX-Large" ForeColor="#009900" Visible="<%# Success %>"></asp:Label>
        </TutorialTemplate>
    </uh:Blockly>
    <asp:SqlDataSource ID="SqlDataSourceSolution" runat="server" ProviderName="System.Data.SQLite"
        SelectCommand="SELECT PhoneNumber FROM People WHERE LENGTH(LastName) <= 5 ORDER BY Birthdate">
    </asp:SqlDataSource>
</body>
</html>
