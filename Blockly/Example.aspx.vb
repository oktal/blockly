﻿Imports System.Xml

Public Class Example
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Protected Sub Blockly_Ready() Handles BlocklyControl.Ready
        If BlocklyControl.ResultContainer.XmlOutput <> "" Then
            Dim blocklyXml As New XmlDocument
            blocklyXml.LoadXml(BlocklyControl.ResultContainer.XmlOutput)
            Dim nsMgr As New XmlNamespaceManager(blocklyXml.NameTable)
            nsMgr.AddNamespace("b", blocklyXml.DocumentElement.NamespaceURI)
            Dim MultiView1 = CType(BlocklyControl.ResultContainer.FindControl("MultiView1"), MultiView)
            For Each condition As XmlElement In Xml1.Document.GetElementsByTagName("condition")
                Dim source = condition.GetAttribute("source")
                Dim destination = condition.GetAttribute("destination")
                Dim xpath = condition.InnerText
                Dim match As Boolean
                Try
                    match = source Is Nothing Or Convert.ToInt32(source) = MultiView1.ActiveViewIndex
                Catch ex As FormatException
                    match = True
                End Try
                If match Then
                    If blocklyXml.DocumentElement.SelectNodes(xpath, nsMgr).Count > 0 Then
                        MultiView1.ActiveViewIndex = Convert.ToInt32(destination)
                    End If
                End If
            Next
        End If
    End Sub

End Class